﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeWebsite.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Rating { get; set; }
        [Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        [Required]
        public int RecipeId { get; set; }
        public virtual Recipe Recipe { get; set; }

    }
}
