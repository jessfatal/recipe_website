﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeWebsite.Models
{
    public class Follow
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public int FollowerId { get; set; }

        [ForeignKey("User")]
        public int FollowingId { get; set; }

        public virtual User Follower { get; set; }

        public virtual User Following { get; set; }
    }
}
