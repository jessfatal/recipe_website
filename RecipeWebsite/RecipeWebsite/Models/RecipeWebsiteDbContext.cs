﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecipeWebsite.Models
{
    public class RecipeWebsiteDbContext : DbContext
    {
        public RecipeWebsiteDbContext(DbContextOptions<RecipeWebsiteDbContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Follow> Follows { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Userfavoriterecipe> Userfavoriterecipes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Follow>()
                .HasOne<User>(u=>u.Follower)
                .WithMany(f => f.Follows)
                .HasForeignKey(u => u.FollowerId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Comment>()
                .HasOne<User>(u => u.User)
                .WithMany(f => f.Comments)
                .HasForeignKey(u => u.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Userfavoriterecipe>()
                .HasOne<User>(u => u.User)
                .WithMany(f => f.Userfavoriterecipes)
                .HasForeignKey(u => u.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
