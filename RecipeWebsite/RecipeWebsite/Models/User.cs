﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;

namespace RecipeWebsite.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [DisplayName("User Name")]
        public string Username { get; set; }
        [Required]
        public string Picture { get; set; }
        
        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public string Description { get; set; }
        [Required]
        [DisplayName("Created At")]
        public DateTime CreatedAt { get; set; }

        public string ReturnUrl { get; set; }
        [NotMapped]
        public virtual IList<AuthenticationScheme> ExternalLogins { get; set; }

        public virtual ICollection<Follow> Follows { get; set; }

        public virtual ICollection<Follow> Followings { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Recipe> Recipes { get; set; }
        public virtual ICollection<Userfavoriterecipe> Userfavoriterecipes { get; set; }

    }
}
