﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RecipeWebsite.Models;

namespace RecipeWebsite.Controllers
{
    public class HomeController : Controller
    {

        private readonly RecipeWebsiteDbContext _context;

        public HomeController(RecipeWebsiteDbContext context)
        {
            _context = context;
        }


        // GET: Recipes
        public async Task<IActionResult> Index()
        {
            var recipeWebsiteDbContext = _context.Recipes.Include(r => r.Category).Include(r => r.User);
            return View(await recipeWebsiteDbContext.ToListAsync());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region API Calls
        [HttpPost]
        public async Task<IActionResult> SetFavorite(int recipeId)
        {
            if (recipeId == null)
            {
                return Json(new { success = false, message = " Invalid data!" });
            }
            int currentUserId = (int)HttpContext.Session.GetInt32("UserId");

            Userfavoriterecipe favorite = new Userfavoriterecipe();
            favorite.UserId = currentUserId;
            favorite.RecipeId = recipeId;
            _context.Userfavoriterecipes.Add(favorite);
            _context.SaveChanges();

            return Json(new { success = true, message = "Successfully collect to favorite!" });
        }
        #endregion
    }
}
