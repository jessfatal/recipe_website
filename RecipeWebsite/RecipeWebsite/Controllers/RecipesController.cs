﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RecipeWebsite.Models;

namespace RecipeWebsite.Controllers
{
    public class RecipesController : Controller
    {
        private readonly RecipeWebsiteDbContext _context;

        public RecipesController(RecipeWebsiteDbContext context)
        {
            _context = context;
        }

        public Recipe Recipe { get; set; }
        // GET: Recipes
        public async Task<IActionResult> Index()
        {
            var recipeWebsiteDbContext = _context.Recipes.Include(r => r.Category).Include(r => r.User);
            return View(await recipeWebsiteDbContext.ToListAsync());
        }

        // GET: Recipes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recipe = await _context.Recipes
                .Include(r => r.Category)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (recipe == null)
            {
                return NotFound();
            }

            return View(recipe);
        }

        // GET: Recipes/Create
        public IActionResult Create()
        {
            //ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Code");
            //ViewData["UserId"] = new SelectList(_context.Users, "Id", "Email");
            List<Category> categories = _context.Categories.ToList();
            ViewBag.Categories = categories;
            return View();
        }

        // POST: Recipes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Ingredients,Directions,Picture,UserId,CategoryId,ImageFile")] Recipe recipe)
        {
            if (true || ModelState.IsValid)
            {
                if (recipe.ImageFile.Length > 0)
                {
                    string filePath = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.IndexOf("\\bin")) + "\\wwwroot\\images\\recipes\\" + recipe.ImageFile.FileName;
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await recipe.ImageFile.CopyToAsync(stream);
                    }
                }

                recipe.Picture = recipe.ImageFile.FileName;
                //create
                recipe.UserId = (int)HttpContext.Session.GetInt32("UserId");
                _context.Add(recipe);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            List<Category> categories = _context.Categories.ToList();
            ViewBag.Categories = categories;
            return View(Recipe);
        }

        // GET: Recipes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recipe = await _context.Recipes.FindAsync(id);
            if (recipe == null)
            {
                return NotFound();
            }
            //ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Code", recipe.CategoryId);
            //ViewData["UserId"] = new SelectList(_context.Users, "Id", "Email", recipe.UserId);
            List<Category> categories = _context.Categories.ToList();
            ViewBag.Categories = categories;
            return View(recipe);
        }

        // POST: Recipes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Ingredients,Directions,Picture,UserId,CategoryId")] Recipe recipe)
        {
            if (id != recipe.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(recipe);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RecipeExists(recipe.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(_context.Categories, "Id", "Code", recipe.CategoryId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Email", recipe.UserId);
            return View(recipe);
        }

        // GET: Recipes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var recipe = await _context.Recipes
                .Include(r => r.Category)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (recipe == null)
            {
                return NotFound();
            }

            return View(recipe);
        }

        // POST: Recipes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var recipe = await _context.Recipes.FindAsync(id);
            _context.Recipes.Remove(recipe);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RecipeExists(int id)
        {
            return _context.Recipes.Any(e => e.Id == id);
        }
    }
}
