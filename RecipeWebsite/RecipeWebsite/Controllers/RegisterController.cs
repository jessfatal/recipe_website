﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RecipeWebsite.Models;
using System.IO;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace RecipeWebsite.Controllers
{
    public class RegisterController : Controller
    {
        private readonly RecipeWebsiteDbContext _db;
        [BindProperty]
        public User User { get; set; }
        public RegisterController(RecipeWebsiteDbContext db)
        {
            _db = db;
        }

        public IActionResult Index(int? id)
        {
            //List<Pet> pets = new List<Pet>();
            //for{
            //    pets.Add()
            //}
            User = new User();
            if (id == null)
            {
                //create
                return View(User);
            }
            //update
            User = _db.Users.FirstOrDefault(u => u.Id == id);
            if (User == null)
            {
                return NotFound();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index()
        {
            if (true || ModelState.IsValid)
            {
                if (User.Id == 0)
                {
                    if (User.ImageFile.Length > 0)
                    {
                        string filePath = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.IndexOf("\\bin")) + "\\wwwroot\\images\\users\\" + User.ImageFile.FileName;
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            User.ImageFile.CopyToAsync(stream);
                        }
                    }

                    User.Picture = User.ImageFile.FileName;
                    //create
                    _db.Users.Add(User);
                }
                else
                {
                    _db.Users.Update(User);
                }
                _db.SaveChanges();
                
               

                return View("Reg_Success", User);
            }
            return View(User);
        }
    }
}
