﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RecipeWebsite.Models;

namespace RecipeWebsite.Controllers
{
    public class ProfileController : Controller
    {
        private readonly RecipeWebsiteDbContext _db;
        [BindProperty]
        public User User { get; set; }
        public ProfileController(RecipeWebsiteDbContext db)
        {
            _db = db;
        }
        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
        }

        public IActionResult About()
        {
            HttpContext.Session.SetString("CurrentPage", "About");
            string email = HttpContext.Session.GetString("Email");
            if (email == null || email == "")
            {
                return RedirectToAction("Index", "Login");
            }
            var user = _db.Users.FirstOrDefaultAsync(u => u.Email == email);
            return View(user.Result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> About(int id, [Bind("Id,Email,Password,Username,Picture,Descrption")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _db.Update(user);
                    await _db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json(new { success = true, message = "Update Successful!" });
            }
            return View(user);
        }

        public IActionResult Index()
        {
            //int userId = (int)HttpContext.Session.GetInt32("UserId");
            HttpContext.Session.SetString("CurrentPage", "ProIndex");
            int userId = 1;
            if (userId == null)
            {
                return RedirectToAction("Index", "Login");
            }
            List<Userfavoriterecipe> favoriterecipes = _db.Userfavoriterecipes.Include(u => u.User).Include(r => r.Recipe).Where(u => u.UserId == userId).ToList();
            foreach (var favoriterecipe in favoriterecipes)
            {
                int recipeCreateBy = favoriterecipe.Recipe.UserId;
                favoriterecipe.Recipe.User = _db.Users.Find(recipeCreateBy);
                favoriterecipe.Recipe.User.Follows = _db.Follows.Where(c => c.FollowingId == recipeCreateBy).ToList();
                favoriterecipe.Recipe.User.Userfavoriterecipes = _db.Userfavoriterecipes.Where(c => c.UserId == recipeCreateBy).ToList();
                favoriterecipe.Recipe.User.Recipes = _db.Recipes.Where(c => c.UserId == recipeCreateBy).ToList();
                favoriterecipe.Recipe.Comments = _db.Comments.Where(c => c.RecipeId == favoriterecipe.RecipeId).ToList();
            }
            return View("Index", favoriterecipes);
        }

        public IActionResult Friends()
        {
            HttpContext.Session.SetString("CurrentPage", "Friends");
            //int userId = (int)HttpContext.Session.GetInt32("UserId");
            int userId = 1;
            if (userId == null)
            {
                return RedirectToAction("Index", "Login");
            }
            List<User> users = _db.Users.ToList();
            return View("Friends", users);
        }

        [Route("Profile/Friends/Following")]
        public IActionResult Following()
        {
            //int userId = (int)HttpContext.Session.GetInt32("UserId");
            int userId = 1;
            if (userId == null)
            {
                return RedirectToAction("Index", "Login");
            }
            List<Follow> follows = _db.Follows.Include(u => u.Following).Where(u => u.FollowerId == userId).ToList();
            return View("Following", follows);
        }

        [Route("Profile/Friends/Follower")]
        public IActionResult Follower()
        {
            //int userId = (int)HttpContext.Session.GetInt32("UserId");
            int userId = 1;
            if (userId == null)
            {
                return RedirectToAction("Index", "Login");
            }
            List<Follow> follows = _db.Follows.Include(u => u.Follower).Where(u => u.FollowingId == userId).ToList();
            return View("Follower", follows);
        }

        public IActionResult MyReviews()
        {
            HttpContext.Session.SetString("CurrentPage", "MyReviews");
            //int userId = (int)HttpContext.Session.GetInt32("UserId");
            int userId = 1;
            if (userId == null)
            {
                return RedirectToAction("Index", "Login");
            }
            List<Comment> comments = _db.Comments.Include(u => u.User).Include(r => r.Recipe).Where(u => u.UserId == userId).ToList();
            return View("MyReviews", comments);
        }
        public IActionResult PersonalRecipes()
        {
            HttpContext.Session.SetString("CurrentPage", "PersonalRecipes");
            //int userId = (int)HttpContext.Session.GetInt32("UserId");
            int userId = 1;
            if (userId == null)
            {
                return RedirectToAction("Index", "Login");
            }
            List<Recipe> personalRecipes = _db.Recipes.Include(u => u.User).Include(c => c.Category).Where(u => u.UserId == userId).ToList();
            return View("PersonalRecipes", personalRecipes);
        }

        public IActionResult PersonalRecipeAdd()
        {
            return View();
        }

        private bool UserExists(int id)
        {
            return _db.Users.Any(e => e.Id == id);
        }

        #region API Calls
        [HttpPost]
        public async Task<IActionResult> SetFollow(int followingId)
        {
            if (followingId == null)
            {
                return Json(new { success = false, message = " Invalid data!" });
            }
            int followerId = (int)HttpContext.Session.GetInt32("UserId");

            Follow follow = new Follow();
            follow.FollowerId = followerId;
            follow.FollowingId = followingId;
            _db.Follows.Add(follow);
            _db.SaveChanges();

            return Json(new { success = true, message = "Successfully set the Following!" });
        }

        public async Task<IActionResult> CancelFollowing(int id)
        {
            if (id == null)
            {
                return Json(new { success = false, message = " Invalid data!" });
            }
            var follow = await _db.Follows.FindAsync(id);
            _db.Follows.Remove(follow);
            _db.SaveChanges();
            return Json(new { success = true, message = "Successfully cancel the Following!" });
        }

        [HttpPost]
        public async Task<IActionResult> Rating(int recipeId, int ratingNum, string description)
        {
            if (recipeId == null)
            {
                return Json(new { success = false, message = " Invalid data!" });
            }

            Comment comment = new Comment();
            comment.UserId = (int)HttpContext.Session.GetInt32("UserId");
            comment.Rating = ratingNum;
            comment.Description = description;
            comment.RecipeId = recipeId;
            _db.Comments.Add(comment);
            _db.SaveChanges();

            return Json(new { success = true, message = "Successfully Comment!" });
        }
        #endregion
    }
}
