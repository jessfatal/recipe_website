using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RecipeWebsite.Models;

namespace RecipeWebsite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //entity framework.tools
            //add-migration AddRecipeWebsiteToDb
            //update-database
            services.AddDbContext<RecipeWebsiteDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddControllersWithViews();
            // Added below Name for session Use
            services.AddMvc().AddSessionStateTempDataProvider();
            //services.AddMvc(options => {
            //    var policy = new AuthorizationPolicyBuilder()
            //                    .RequireAuthenticatedUser()
            //                    .Build();
            //    options.Filters.Add(new AuthorizeFilter(policy));
            //}).AddXmlDataContractSerializerFormatters();

            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<RecipeWebsiteDbContext>();

            services.AddAuthentication()
                .AddGoogle("google", Options =>
                {
                    Options.ClientId = "449880986838-orskbk56be0ver5p8emicj00esmd89sm.apps.googleusercontent.com";
                    Options.ClientSecret = "ofPAUpAfKP8ikZG3FvyK4zc0";
                    //Options.SignInScheme = IdentityConstants.ExternalScheme;
                });
            //services.AddScoped<SignInManager<RecipeWebsite.Models.User>, SignInManager<RecipeWebsite.Models.User>>();
            services.AddSession();
            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            //Added below Name for session Use
            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapControllerRoute(
                //    name: "test",
                //    pattern: "signin-google",
                //    defaults: new { controller = "Login", action = "ExternalLoginCallback" }
                //);
                endpoints.MapControllerRoute(
                   name: "profile",
                   pattern: "profile",
                   defaults: new { controller = "Profile", action = "Index" }
                   );

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
   
            });
        }
    }
}
